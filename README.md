Sandbox project
===============

Install raw-php project
-----------------------

```
$ docker-compose build
$ ./bin/composer install
```

Exercices
---------

* In a new folder in src/ with your name : 
    1. Implement a ShippingEventListener and a ShippingEvent
    1. Test order status is correctly updated to shipping