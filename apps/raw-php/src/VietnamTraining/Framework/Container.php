<?php

namespace VietnamTraining\Framework;

class Container
{
    /** @var EventDispatcher */
    protected $eventDispatcher;

    public function getEventDispatcher(): EventDispatcher
    {
        if(!$this->eventDispatcher) {
            $this->eventDispatcher = new EventDispatcher();
        }

        return $this->eventDispatcher;
    }

    public static function getTime()
    {
        $this->eventDispatcher;

        return time();
    }
}
