<?php


namespace VietnamTraining\Framework;


use VietnamTraining\Order\OrderInterface;

interface EventInterface
{
    public function getName(): string;

    public function getOrder(): OrderInterface;

    public function getAdditionalData(): array;
}
