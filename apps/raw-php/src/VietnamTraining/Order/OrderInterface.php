<?php


namespace VietnamTraining\Order;


interface OrderInterface
{
    const STATUS_NEW = 'new';
    const STATUS_SHIPPING = 'shipping';

    public function getOrderReference(): string;

    public function getProducts(): array;

    public function getAmount(): float;

    public function getPaidAmount(): float;

    public function getStatus(): string;

    public function setStatus(string $status);

    public function getCountry(): string;
}
