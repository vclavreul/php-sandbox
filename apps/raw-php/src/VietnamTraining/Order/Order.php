<?php


namespace VietnamTraining\Order;


class Order implements OrderInterface
{
    /** @var string */
    protected $orderReference;

    /** @var array */
    protected $products;

    /** @var float */
    protected $amount;

    /** @var float */
    protected $paidAmount;

    /** @var string */
    protected $status;

    /** @var string */
    protected $country;

    /**
     * Order constructor.
     *
     * @param string $orderReference
     * @param array  $products
     * @param float  $amount
     * @param float  $paidAmount
     * @param string $status
     * @param string $country
     */
    public function __construct(string $orderReference, array $products, float $amount, float $paidAmount, string $status, string $country)
    {
        $this->orderReference = $orderReference;
        $this->products = $products;
        $this->amount = $amount;
        $this->paidAmount = $paidAmount;
        $this->status = $status;
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getOrderReference(): string
    {
        return $this->orderReference;
    }

    /**
     * @return array
     */
    public function getProducts(): array
    {
        return $this->products;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @return float
     */
    public function getPaidAmount(): float
    {
        return $this->paidAmount;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }
}
