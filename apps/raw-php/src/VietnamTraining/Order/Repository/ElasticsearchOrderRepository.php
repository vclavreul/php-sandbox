<?php


namespace VietnamTraining\Order\Repository;


use VietnamTraining\Order\Exception\CannotSaveOrderException;
use VietnamTraining\Order\OrderInterface;

class ElasticsearchOrderRepository implements OrderRepositoryInterface
{

    /** @var OrderRepositoryInterface */
    protected $repository;

    /**
     * ElasticsearchOrderRepository constructor.
     *
     * @param OrderRepositoryInterface $repository
     */
    public function __construct(OrderRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }


    public function saveOrder(OrderInterface $order)
    {
        $this->repository->saveOrder($order);

        // Do ES indexation
    }

    public function findOrder(string $identifier): ?OrderInterface
    {
        return null; // Get from ES
    }

}
