<?php


namespace VietnamTraining\Order\Repository;


use VietnamTraining\Order\Exception\CannotSaveOrderException;
use VietnamTraining\Order\OrderInterface;

class ArrayOrderRepository implements OrderRepositoryInterface
{
    /** @var OrderInterface[] */
    protected $orders = [];

    public function saveOrder(OrderInterface $order)
    {
        $this->orders[$order->getOrderReference()] = clone $order;
    }

    public function findOrder(string $identifier): ?OrderInterface
    {
        if (!array_key_exists($identifier, $this->orders)) {
            return null;
        }

        return clone $this->orders[$identifier];
    }

}
