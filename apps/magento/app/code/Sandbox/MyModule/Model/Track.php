<?php


namespace Sandbox\MyModule\Model;


use Psr\Log\LoggerInterface;

class Track implements TrackInterface
{
    protected $data;

    /** @var LoggerInterface */
    protected $logger;

    /**
     * Track constructor.
     *
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }


    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    public function play()
    {
        $this->logger->info('Playing a track !');
    }
}
