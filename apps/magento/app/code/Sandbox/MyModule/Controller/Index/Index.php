<?php


namespace Sandbox\MyModule\Controller\Index;


use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultInterface;
use Sandbox\MyModule\Model\TrackInterfaceFactory;
use Sandbox\MyModule\Model\TrackInterface;

class Index implements ActionInterface
{

    /** @var JsonFactory */
    protected $jsonResponseFactory;

    /** @var TrackInterfaceFactory */
    protected $trackFactory;

    /**
     * Index constructor.
     *
     * @param JsonFactory  $jsonResponseFactory
     * @param TrackInterfaceFactory $trackFactory
     */
    public function __construct(JsonFactory $jsonResponseFactory, TrackInterfaceFactory $trackFactory)
    {
        $this->jsonResponseFactory = $jsonResponseFactory;
        $this->trackFactory = $trackFactory;
    }


    /**
     * @return ResponseInterface|ResultInterface
     */
    public function execute()
    {
        /** @var TrackInterface $track */
        $track = $this->trackFactory->create();
        $track->setData([
            'name' => 'hello',
            'duration' => 12,
        ]);

        $track->play();

        $response = $this->jsonResponseFactory->create();
        $response->setData($track->getData());

        return $response;
    }

}
