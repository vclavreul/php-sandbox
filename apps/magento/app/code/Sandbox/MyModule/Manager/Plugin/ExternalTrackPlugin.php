<?php


namespace Sandbox\MyModule\Manager\Plugin;


use Psr\Log\LoggerInterface;
use Sandbox\MyModule\Manager\TrackManager;
use Sandbox\MyModule\Manager\TrackManagerInterface;

class ExternalTrackPlugin
{

    /** @var LoggerInterface */
    protected $logger;

    /**
     * ExternalTrackPlugin constructor.
     *
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Before plugin for
     * @see TrackManagerInterface::getTracks
     *
     * @param TrackManagerInterface $subject
     *
     */
    public function beforeGetTracks(TrackManagerInterface $subject)
    {
        $subject->setTracks([
            [
                'name' => 'My new track',
            ],
        ]);
    }


    /**
     * After Plugin for
     * @see TrackManagerInterface::getTracks
     *
     * @param TrackManagerInterface $subject
     * @param array                 $result
     *
     * @return array
     *
     */
    public function afterGetTracks(
        TrackManagerInterface $subject,
        array $result
    ) {
        $result[] = [
            'name' => 'External Track',
        ];

        return $result;
    }

    /**
     * Around plugin for
     * @see TrackManagerInterface::getTracks
     *
     * @param TrackManagerInterface|TrackManager $subject
     * @param \Closure              $methodCall
     *
     * @return mixed
     */
    public function aroundGetTracks(TrackManagerInterface $subject, \Closure $methodCall): array
    {
        // Do something here
        $this->logger->info('There is '.count($subject->tracks).' Tracks');

        $result = $methodCall();

        // Do something else here
        $this->logger->info('There is '.count($subject->tracks).' Tracks');

        return $result;
    }

}
