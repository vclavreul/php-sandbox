<?php


namespace Sandbox\MyModule\Manager;


interface TrackManagerInterface
{
    public function setTracks(array $tracks);

    public function getTracks(): array;

}
